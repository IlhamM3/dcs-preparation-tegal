#!/bin/sh

if [ $(id -u) -ne "0" ]; then
	echo "Please run as root"
	exit 1
fi

echo "blacklist pn533_usb" | tee -a /etc/modprobe.d/blacklist.conf
echo "blacklist pn533" | tee -a /etc/modprobe.d/blacklist.conf
echo "blacklist nfc" | tee -a /etc/modprobe.d/blacklist.conf
echo "install nfc /bin/false" | tee -a /etc/modprobe.d/blacklist.conf
echo "install pn533 /bin/false" | tee -a /etc/modprobe.d/blacklist.conf
echo "install pn533_usb /bin/false" | tee -a /etc/modprobe.d/blacklist.conf

if [ ! -z $(lsmod | grep -w pn533_usb) ]; then
	rmmod pn533_usb
fi

if [ ! -z $(lsmod | grep -w pn533) ]; then
	rmmod pn533
fi

if [ ! -z $(lsmod | grep -w nfc) ]; then
	rmmod nfc
fi

apt install libpcsclite1 libpcsclite-dev pcscd
