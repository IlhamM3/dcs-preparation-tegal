#!/bin/bash

set -e
set -x


if [ "$EUID" -ne 0 ]; then
	echo "Please run as root"
	exit 1
fi

HOSTNAME=$(hostname -f)

sed -i "s/${HOSTNAME}/${1}/g" /etc/hostname 
sed -i "s/${HOSTNAME}/${1}/g" /etc/hosts 

