#!/bin/bash

set -e
set -x

if [ "$EUID" -ne 0 ]; then
	echo "Please run as root"
	exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ASSETS_DIR=${DIR}"/../assets"

if [[ -z ${1} ]]; then
	echo -e "Usage:\n $0 <CLIENT_IP>\n\nexample:\n $0 10.10.1.8\n\n"
	exit -1
fi

if [[ $(echo ${1} | grep -Po "10.10.1.[0-9]+") ]]; then
	IP_CLIENT=${1}
else
	IP_CLIENT=
fi

while [[ -z ${IP_CLIENT} ]]; do
	read -p "VPN IP CLIENT [10.10.0.xxx] : " IP_CLIENT

	if [[ -z $(echo ${IP_CLIENT} | grep -Po "10.10.1.[0-9]+") ]]; then
		IP_CLIENT=
	fi
done

LAST_IP="$(echo "${IP_CLIENT}" | cut -d "." -f 4)"

IP_CLIENT2="172.16.10.${LAST_IP}"

# INSTALL SOFTETHER VPN CLIENT
echo '====================== INSTALLING SOFETHER VPNCLIENT ======================'
tar xzf ${ASSETS_DIR}/softether-vpnclient-x64.tar.gz
mv vpnclient/ /usr/local
cd /usr/local/vpnclient && printf "1\n1\n1\n" | make && /usr/local/vpnclient/vpnclient start

/usr/local/vpnclient/vpncmd /CLIENT 127.0.0.1 /CMD NicCreate vpn
printf "${ASSETS_DIR}/raspi.vpn\n" | /usr/local/vpnclient/vpncmd /CLIENT 127.0.0.1 /CMD AccountImport
/usr/local/vpnclient/vpncmd /CLIENT 127.0.0.1 /CMD AccountConnect raspi
/usr/local/vpnclient/vpncmd /CLIENT 127.0.0.1 /CMD AccountStartupSet raspi

/usr/local/vpnclient/vpncmd /CLIENT 127.0.0.1 /CMD NicCreate srv4
printf "${ASSETS_DIR}/srv4.stechoq.com.vpn\n" | /usr/local/vpnclient/vpncmd /CLIENT 127.0.0.1 /CMD AccountImport
/usr/local/vpnclient/vpncmd /CLIENT 127.0.0.1 /CMD AccountConnect srv4.stechoq.com
/usr/local/vpnclient/vpncmd /CLIENT 127.0.0.1 /CMD AccountStartupSet srv4.stechoq.com

cat ${ASSETS_DIR}/vpnclient | tee /etc/init.d/vpnclient > /dev/null

chmod +x /etc/init.d/vpnclient

sed -i "s/_VPN_IP_CLIENT_/${IP_CLIENT}/g" /etc/init.d/vpnclient
sed -i "s/_VPN_IP_CLIENT2_/${IP_CLIENT2}/g" /etc/init.d/vpnclient

update-rc.d vpnclient defaults
service vpnclient start
