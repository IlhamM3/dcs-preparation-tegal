module.exports = {
	serial: {
		port: '/dev/ttyUSB0',
		baud: 38400,
	},

	showCursor: true,

    machineId: 16,

	devTools: true,

    rfid: {
        ip: "192.168.253.200",
        port: 502,
        id: 255,
        timeout: 1000,
    },

    window : {
		width : 1280,
		height : 800,
		fullscreen : false,
	},

	api: {
		baseurl: 'http://insens.tmp.stechoq.com',
		username: 'contoh',
		password: 'password',
		isRegistered: true,
	},

	autoreload : true,

	debug : true,
}