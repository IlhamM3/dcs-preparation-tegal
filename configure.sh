#!/bin/bash

#save script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ASSETS_DIR=${DIR}"/assets"

#git directory
GIT_DIR="/opt/sugity-dcs-device"

#git branch
if [[ ! -z $1 ]]; then
	GIT_BRANCH=$1
fi

#log file
LOGFILE="/home/$USER/.sugity_dcsConfigured"

#shortcut to start x11vnc
X11VNC_SHORTCUT="/opt/x11vnc.sh"

if [ -f "$LOGFILE" ]; then
	rerun=1
fi

#check if installed OS is Lubuntu 20.04
isLubuntu=$(cat /var/log/installer/media-info | grep -io "Lubuntu")
isFocal=$(lsb_release -r | grep -o "20.04" | tail -1)

if [[ -z $isLubuntu || -z $isFocal ]]; then
	echo -e "\e[95m"
	echo "=================================================================================="
	echo "Must Be Lubuntu 20.04. Re-Install this computer with Lubuntu 20.04 Focal fossa  "
	echo ""
	echo "Current Installation :"
	echo "$(cat /var/log/installer/media-info | grep -i 'buntu')"
	echo "$(lsb_release -r)"
	echo "=================================================================================="
	echo -e "\e[49m"
	exit 1
fi

#check internet connection, if no connection available exit then shutdown
wget -q --spider https://gitlab.com

if [ $? -eq 0 ]; then
	echo "Internet Connection Available"
else
	echo "No Internet Connection Available"
	echo "Connect to Internet then restart this device"
	sleep 10
	poweroff
	exit 1
fi

#ask user to input password for sudo
#sudo -k to reset invalid attempt
read -rs -p "Input Password for [$USER]: " OSPassword
sudo -k
#repeat until password is correct
while ! sudo -lS &> /dev/null <<< $OSPassword; do
	echo "Wrong password!"
	read -rs -p "Input Password for [$USER]: " OSPassword
	sudo -k
done

#~ clear

#add to group dialout
printf "===== Adding user '$USER' to group 'dialout' =====\n"
printf "%s\n" "$OSPassword" | sudo --stdin usermod -a -G dialout $USER

#added nopasswd to sudoers
sudoersFile=/etc/sudoers
sudoersText="$USER     ALL=(ALL) NOPASSWD:ALL"
printf "$sudoersText\n" | sudo tee -a $sudoersFile > /dev/null

#update and install
printf "\n===== Running System Update =====\n"

#change source.list to local repo
printf "deb http://mirror.deace.id/ubuntu/ focal main restricted\n
deb http://mirror.deace.id/ubuntu/ focal-updates main restricted\n
deb http://mirror.deace.id/ubuntu/ focal universe\n
deb http://mirror.deace.id/ubuntu/ focal-updates universe\n
deb http://mirror.deace.id/ubuntu/ focal multiverse\n
deb http://mirror.deace.id/ubuntu/ focal-updates multiverse\n
deb http://mirror.deace.id/ubuntu/ focal-backports main restricted universe multiverse\n
deb http://mirror.deace.id/ubuntu/ focal-security main restricted\n
deb http://mirror.deace.id/ubuntu/ focal-security universe\n
deb http://mirror.deace.id/ubuntu/ focal-security multiverse\n" | sudo tee /etc/apt/sources.list > /dev/null

#remove unused software
printf "Removing software that's not needed for operation\n"
printf "%s\n" "$OSPassword" | sudo --stdin apt remove --purge -y firefox 2048-qt featherpad vlc k3b libreoffice-base-core libreoffice-common libreoffice-draw libreoffice-help-common libreoffice-impress libreoffice-qt5 libreoffice-style-colibre libreoffice-writer libreoffice-calc libreoffice-core libreoffice-gtk3 libreoffice-help-en-us libreoffice-math libreoffice-style-breeze libreoffice-style-tango transmission-common transmission-qt trojita qpdfview snapd

printf "%s\n" "$OSPassword" | sudo --stdin apt update -y

# if [[ "$doUpgrade" == "y" || "$doUpgrade" == "Y" ]]; then
# 	printf "===== Running System Upgrade =====\n"
# 	printf "%s\n" "$OSPassword" | sudo --stdin apt upgrade -y
# fi

printf "%s\n" "$OSPassword" | sudo --stdin apt install --fix-missing -y
printf "%s\n" "$OSPassword" | sudo --stdin apt install -y nano git nodejs npm x11vnc openssh-server net-tools nmap

#sometimes installation is broken, try running it again with --fix-missing first
printf "%s\n" "$OSPassword" | sudo --stdin apt install --fix-missing -y
printf "%s\n" "$OSPassword" | sudo --stdin apt install -y nano git nodejs npm x11vnc openssh-server net-tools nmap

#~ clear

#install nodejs v14 lts
printf "===== Installing nodejs v14 lts =====\n"
sudo npm i n -g
sudo n v14

#~ clear

if [ -z $rerun ]; then
	printf "===== Configuring Lubuntu Settings =====\n"

	#disabling some autostarts by moving them to another directory instead of removing them
	mkdir -p ~/.notused \
	&& sudo mv -t ~/.notused \
	/etc/xdg/autostart/lxqt-panel.desktop \
	/etc/xdg/autostart/lxqt-notifications.desktop \
	/etc/xdg/autostart/lxqt-powermanagement.desktop \
	/etc/xdg/autostart/lxqt-runner.desktop \
	/etc/xdg/autostart/lxqt-qlipper-autostart.desktop \
	/etc/xdg/autostart/upg-notifier-autostart.desktop \
	/etc/xdg/autostart/snap-userd-autostart.desktop

	#copy wallpaper image
	sudo cp $DIR/img/wallpaper.png /usr/share/lubuntu/wallpapers/sugity_dcs-wallpaper.png

	#copy xscreensaver profile, to prevent screen turned off
	cp -rf $DIR/profile/.xscreensaver /home/$USER/.xscreensaver

	#overwrite pcmanfm-qt lxqt profile
	cp -rf $DIR/profile/settings.conf /etc/xdg/xdg-Lubuntu/pcmanfm-qt/lxqt/settings.conf

	#set number of workspace to 1 by overwriting openbox config with preset value
	sudo cp -rf $DIR/profile/lxqt-rc.xml /etc/xdg/xdg-Lubuntu/openbox/lxqt-rc.xml

	#remove desktop icon
	sudo sed -i 's/Wallpaper\=.*/Wallpaper\=\/usr\/share\/lubuntu\/wallpapers\/sugity_dcs-wallpaper\.png/g' /etc/xdg/xdg-Lubuntu/pcmanfm-qt/lxqt/settings.conf
	sudo sed -i 's/WallpaperMode\=.*/WallpaperMode\=fit/g' /etc/xdg/xdg-Lubuntu/pcmanfm-qt/lxqt/settings.conf
	sudo sed -i 's/DesktopShortcuts\=.*/DesktopShortcuts\=\@Invalid\(\)/g' /etc/xdg/xdg-Lubuntu/pcmanfm-qt/lxqt/settings.conf

	#copy config to user's folder
	cp -rf /etc/xdg/xdg-Lubuntu/openbox ~/.config
	cp -rf /etc/xdg/xdg-Lubuntu/pcmanfm-qt ~/.config

	rm -rf ~/Desktop/*.desktop

	#Create shortcut to x11vnc
	printf "x11vnc -display :0 -auth ~/.Xauthority" | sudo tee $X11VNC_SHORTCUT > /dev/null
	sudo chown $USER:$USER $X11VNC_SHORTCUT
	sudo chmod 755 $X11VNC_SHORTCUT

	#~ clear

	#create Autologin user in sddm desktop manager
	printf "Setting Autologin for [$USER]\n"
	printf "[Autologin]\nUser=$USER\nSession=Lubuntu\n\n" | sudo tee /etc/sddm.conf > /dev/null

	#changing to custom plymouth by editing default.plymouth
	printf "===== Setting Plymouth =====\n"
	printf "%s\n" "$OSPassword" | sudo --stdin cp -rf $DIR/plymouth/themes/sugity_dcs /usr/share/plymouth/themes/sugity_dcs
	printf "%s\n" "$OSPassword" | sudo --stdin update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/sugity_dcs/sugity_dcs.plymouth 100
	cat /usr/share/plymouth/themes/sugity_dcs/sugity_dcs.plymouth | sudo tee /usr/share/plymouth/themes/default.plymouth > /dev/null
	printf "%s\n" "$OSPassword" | sudo --stdin update-initramfs -u

	#change grub timeout on recordfail
	sudo sed -i '/GRUB_RECORDFAIL_TIMEOUT/d' /etc/default/grub
	printf "GRUB_RECORDFAIL_TIMEOUT=0\n" | sudo tee -a /etc/default/grub > /dev/null && sudo update-grub

	#change ssh config
	#change port from default 22 to 65432
	sudo sed -i 's/#Port.*/Port 65432/g' /etc/ssh/sshd_config

	#change password auth to no
	#~ sudo sed -i 's/#PasswordAuthentication.*/PasswordAuthentication no/g' /etc/ssh/sshd_config

	#~ clear
fi

#copy default authorized pubkey
mkdir -p ~/.ssh
cp -rf $DIR/profile/authorized_keys ~/.ssh/authorized_keys
#copy ssh key to clone gitlab repo
cp -rf $DIR/profile/rsa/sugity_dcs_rsa ~/.ssh/id_rsa
cp -rf $DIR/profile/rsa/sugity_dcs_rsa.pub ~/.ssh/id_rsa.pub
#set permission
chmod 700 ~/.ssh
chmod 600 ~/.ssh/*

#add gitlab.com to known_host
ssh-keygen -F gitlab.com || ssh-keyscan gitlab.com >>~/.ssh/known_hosts

#restart ssh service
sudo service sshd restart

#clone repo and checkout used branch
if [[ -z $GIT_BRANCH ]]; then
	GIT_BRANCH="master"
fi

printf "==== Initializing sugity dcs device and checkout branch '$GIT_BRANCH' =====\n"

#if git folder already exist, delete it
if [[ -d $GIT_DIR ]]; then
	sudo rm -rf $GIT_DIR
fi

#Create dircetory for git clone
sudo mkdir -p $GIT_DIR
sudo chown $USER:$USER $GIT_DIR
sudo chmod 755 $GIT_DIR

#clone repo on specific GIT_BRANCH into folder
GIT_HTTPS_URL="https://gitlab.com/IlhamM3/DCS-Electron.git"
GIT_SSH_URL="git@gitlab.com:IlhamM3/DCS-Electron.git"

GIT_URL=$GIT_SSH_URL

git clone -b $GIT_BRANCH $GIT_URL $GIT_DIR

#set shell files as executable
cd $GIT_DIR && chmod +x $GIT_DIR/bash/*.sh

#install node modules
cd $GIT_DIR && npm i

#check if npm installation run succesfully by checking modules listed in package.json and compare them with npm ls
echo "Checking npm package installation..."
PACKAGE_JSON=$GIT_DIR/package.json
PACKAGE_JSON_CONTENT=$(cat $PACKAGE_JSON)

devDependencies=$(echo $PACKAGE_JSON_CONTENT | sed -r 's/\ //g' | grep -Po '"devDependencies"([^}]+)' | sed -r 's/\"//g' | sed -r 's/devDependencies([^{]+)\{//g' | sed -r 's/[\,]/\n/g' | sed -r 's/\:([^\n]+)//g')
arrDevDependencies=(${devDependencies//\ / })

dependencies=$(echo $PACKAGE_JSON_CONTENT | sed -r 's/\ //g' | grep -Po '"dependencies"([^}]+)' | sed -r 's/\"//g' | sed -r 's/dependencies([^{]+)\{//g' | sed -r 's/[\,]/\n/g' | sed -r 's/\:([^\n]+)//g')
arrDependencies=(${dependencies//\ / })

arrBindings=("@serialport/bindings@" "@serialport/binding-mock@" "@serialport/binding-abstract@")

packages=("${arrDependencies[@]}" "${arrDevDependencies[@]}" "${arrBindings[@]}")

echo "Checking npm package installation..."
NPM_LIST=$(cd $GIT_DIR && npm ls)
for item in ${packages[@]}; do
	check=$(echo $NPM_LIST | grep -o "${item}@" | tail -1)
	if [[ -z $check ]]; then
		PACKAGE_NOT_INSTALLED=${item}
		break
	fi
done

#change git URL to https. Using '#' as delimiter because URLs contain '/' characters
sed -i "s#$GIT_SSH_URL#$GIT_HTTPS_URL#g" $GIT_DIR/.git/config

#remove ssh key
rm -rf ~/.ssh/id_rsa
rm -rf ~/.ssh/id_rsa.pub
rm -rf ~/.ssh/known_hosts

#~ clear

#copy config sample in order to get application running
#cp $GIT_DIR/config/app.config.sample.js $GIT_DIR/config/app.config.js
cp ${ASSETS_DIR}/app.config.js $GIT_DIR/config/app.config.js


#Set config
printf "Would you like to edit app.config.js "
read -t 10 -n 1 -p "(Y/n)?" editAppConfig

if [[ "$editAppConfig" = "y" || "$editAppConfig" = "Y" ]]; then
	vi $GIT_DIR/config/app.config.js
fi

#~ clear

#create autostart
printf "===== Creating autostart ====="

AUTOSTART_FILEPATH="/opt/start-dcs.sh"

printf "#!/bin/bash\n\
cd ${GIT_DIR} && npm start" | sudo tee ${AUTOSTART_FILEPATH}

sudo chown $USER:$USER ${AUTOSTART_FILEPATH}
sudo chmod 755 ${AUTOSTART_FILEPATH}

printf "[Desktop Entry]\n\
Type=Application\n\
Exec=${AUTOSTART_FILEPATH}\n\
Hidden=false\n\
NoDisplay=false\n\
X-GNOME-Autostart-enabled=true\n\
Name[en_US]=dcs-device\n\
Name=dcs-device\n\
Comment[en_US]=run dcs device\n\
Comment=run dcs device" | sudo tee /etc/xdg/autostart/sugity_dcs.desktop

#create desktop shortcut
printf "===== Creating desktop shortcut ====="

#~ clear

#installing usbguard. Put it at the end of process because it may disable keyboard+mouse
# printf "===== USBGUARD Installation =====\n"
# printf "CAUTION : Your Keyboard + Mouse will be unusable after usbguard installation"
# read -n 1 -t 10 -p "Install usbguard (Y/n)? " installUsbguard

# if [[ "${installUsbguard,,}" != "n" || -z $installUsbguard ]]; then
# 	#usb guard installation
# 	printf "\nInstalling usbguard..."
# 	printf "%s\n" "$OSPassword" | sudo --stdin apt install -y usbguard

# 	#usb guard config
# 	printf "%s\n" "$OSPassword" \
# 	| sudo --stdin sh -c 'printf "allow id 1d6b:0003\nallow id 14e1:3500\nallow id 0590:00c9\nallow id 0424:2514\nallow id 0424:2240\nallow id 0403:6001\nallow id 1908:2070\nallow id 1d6b:0002" > /etc/usbguard/rules.conf'

# 	printf "%s\n" "$OSPassword" | sudo --stdin service usbguard restart

# 	#disabling global keyboard shortcut
# 	mv -t ~/.notused /etc/xdg/autostart/lxqt-globalkeyshortcuts.desktop

# 	#~ clear
# fi

#remove unused libraries
printf "==== Running autoremove =====\n"
printf "%s\n" "$OSPassword" | sudo --stdin apt clean -y
printf "%s\n" "$OSPassword" | sudo --stdin apt autoremove -y

# ============== SET STATIC IP =====================

#~ #use random number from 2...102 instead of asking user to input
#~ ipAddressInput=$(( ( RANDOM % 100 ) + 2 ))

#~ #apply static ip via nmcli. put these lines at the end to avoid disconnecting internet connection
#~ #mod by 100 to make sure ip is not above 100
#~ ipAddressInput=$(($ipAddressInput%100))

#~ #if IP is 0 or 1, change to 2 as 1 and 0 is reserved
#~ if [[ $ipAddressInput = 1 || $ipAddressInput = 0 ]]; then
	#~ ipAddressInput=$(($ipAddressInput + 2))
#~ fi

#~ #assign IP address to be set
#~ ipClass="192.168.88"
#~ ipAddress="$ipClass.$ipAddressInput"
#~ ipGW="$ipClass.1"

#~ #get active device name
#~ #in case multiple active device is listed select the last one only,
#~ #because the procedure is to connect PLC to the last LAN socket, which is enp3s0
#~ ethernetDev=$(nmcli device status | grep -v 'disconnected' | grep 'connected' | grep 'ethernet' | grep -o '^\S*' | tail -1)

#~ #get connection name
#~ conName=$(nmcli -t -f DEVICE,NAME c show --active | grep "$ethernetDev\:" | sed "s/$ethernetDev\://g")

#~ #set IP to static and manual
#~ printf "===== Set Static IP to device \"$ethernetDev\" \"$conName\" =====\n"
#~ nmcli con mod "$conName" ip4 "$ipAddress"/24 gw4 $ipGW ipv4.dns "$ipGW 1.1.1.1 8.8.8.8" ipv4.method "manual"
#~ nmcli con up "$conName"

 #assign IP address RFID to be set on wired conn 2 (center)
 ipClass2="192.168.253" #this class just example
 ipAddress2="$ipClass2.1"

 ipClass3="192.168.252" #this class just example
 ipAddress3="$ipClass3.101"

 printf "===== Set Static IP to device \"$ipAddress\" =====\n"

 sudo nmcli con mod "Wired connection 2" ip4 "$ipAddress2/24" ipv4.method "manual" ipv4.never-default yes ipv4.ignore-auto-routes yes ipv4.ignore-auto-dns yes
sudo nmcli con up "Wired connection 2"

 sudo nmcli con mod "Wired connection 3" ip4 "$ipAddress3/24" ipv4.method "manual" ipv4.never-default yes ipv4.ignore-auto-routes yes ipv4.ignore-auto-dns yes
sudo nmcli con up "Wired connection 3"

#  ipGW="$ipClass.1"

#~ #get active device name
#~ #in case multiple active device is listed select the last one only,
#~ #because the procedure is to connect PLC to the last LAN socket, which is enp3s0
#~ ethernetDev=$(nmcli device status | grep -v 'disconnected' | grep 'connected' | grep 'ethernet' | grep -o '^\S*' | tail -1)

#~ #get connection name
#~ conName=$(nmcli -t -f DEVICE,NAME c show --active | grep "$ethernetDev\:" | sed "s/$ethernetDev\://g")

#~ #set IP to static and manual
#~ printf "===== Set Static IP to device \"$ethernetDev\" \"$conName\" =====\n"
#~ nmcli con mod "$conName" ip4 "$ipAddress"/24 gw4 $ipGW ipv4.dns "$ipGW 1.1.1.1 8.8.8.8" ipv4.method "manual"
#~ nmcli con up "$conName"

# ============== END OF SET STATIC IP =====================

#create log for this installation, also used as a flag that this script has been run before
printf "DIR=\"$GIT_DIR\"\nGIT_BRANCH=\"$GIT_BRANCH\"\nGIT_USER=\"$gitUser\"\nIP_ADDRESS=\"$ipAddress\"\n" | tee $LOGFILE > /dev/null

#clear bash history
history -c
rm -f ~/.bash_history
echo 'history -c' >> ~/.bashrc
echo 'set +o history' >> ~/.bashrc
echo 'history -c' >> ~/.bash_logout

#removing nopasswd
sudo sed -i '/NOPASSWD\:ALL/d' $sudoersFile

#~ clear

printf "Installation Finished.\n"
printf "IP Address : $ipAddress\n"
doReboot="y"
read -t 30 -p "Reboot in 30 Seconds..." doReboot
if [[ "$doReboot" == "y" || "$doReboot" == "Y" || -z $doReboot ]]; then
	rm -rf $DIR
	printf "%s\n" "$OSPassword" | sudo --stdin reboot
else
	printf "\nFinished\n"
	rm -rf $DIR && cd
fi
